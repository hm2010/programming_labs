#include <iostream>
#include <cstdlib>
#include <thread>
#include <mutex>
#include <chrono>

std::mutex g_mutex;

/* MT-NOSYNC */
void f1(void){
    for (int i = 0; i < 100; i++) {
        std::cout << "aaaaa" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}
void f2(void) {
    for (int i = 0; i < 100; i++) {
        std::cout << "bbbbb" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}
/* MT-SYNC */
void g1(void){
    int counter = 0;
    g_mutex.lock();
    for (int i = 0; i < 100; i++) {
        if(counter < 3) {
        	std::cout.flush();
            std::cout << i << "aaaaa" << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            counter++;
        }
        else{
        	std::cout.flush();
            std::cout << i << "aaaaa" << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            counter = 0;
            g_mutex.unlock();
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            g_mutex.lock();
        }
        if (i == 99){ 
            g_mutex.unlock(); 
        }
    }
}
void g2(void){
    int counter = 0;
    g_mutex.lock();
    for (int i = 0; i < 100; i++) {
        if(counter < 3) {
        	std::cout.flush();
            std::cout << i <<"bbbbb" << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            counter++;
        }
        else{
        	std::cout.flush();
            std::cout << i << "bbbbb" << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            counter = 0;
            g_mutex.unlock();
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            g_mutex.lock();

        }
        if (i == 99){
            g_mutex.unlock();
        }
    }
}
int main() {

    {
        std::cout << "MT-NOSYNC" << std::endl;
        std::thread thrf1(f1);
        std::thread thrf2(f2);
        thrf1.join();
        thrf2.join();
    }
    {
        std::cout << "MT-SYNC" << std::endl;
        std::thread thrg1(g1);
        std::thread thrg2(g2);
        thrg1.join();
        thrg2.join();
    }
    {
        std::cout << "MT-WAIT" << std::endl;
    }
    std::cout << "END OF PROGRAM";
    return 0;
}