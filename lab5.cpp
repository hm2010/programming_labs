#include <iostream>
#include <stdexeption>
#include <string>
#include <memory>

using namespace std;

Class RootDirError{};

Class FSElement{
protected:
	shared_ptr<Directory> parentDir;
	string name;
	string path;
public:
	FSElement(shared_ptr<Directory> _parentDir, string _name){
		parentDir = _parentDir;
		name = _name;
		strcat(path, parentDir -> path);
		strcat(path, name);
	}
	virtual ~FSElement();
};

Class File: virtual public FSElement{
private:
	string data;
public:

};

Class Directory: virtual public FSElement{
private:
	vector <weak_ptr<Directory>> fselements;
public:
	static shared_ptr<Directory> rootDir;
	Directory(string _name){
		if (rootDir == nullptr){
			parentDir = nullptr;
			rootDir = this;
			name = _name;
			path = _name;
		}
		else
			throw RootDirError();
	}
	Directory(shared_ptr<Directory> _parentDir, string _name): 
		FSElement(shared_ptr<Directory> _parentDir, string _name){	
	}
	~Directory(){};
};

int main(void){

	return 0;
}