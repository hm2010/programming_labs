#include <iostream>
using namespace std;
//Var17
//
class Square{
protected:
    float lenght;
public:
    Square(){
        lenght = 0;
        cout << "ctor Square (empty), lenght = " << lenght << endl;
    }
    Square(float _lenght){ 
        lenght = _lenght;
        cout << "ctor Square, lenght = " << lenght << endl;
    }
    ~Square(){ 
        cout << "dtor Square" << lenght << endl;
    };
    const float area(){
        cout << "--From Square--" << endl;
        return lenght * lenght;
    };
    virtual const float areaVirt(){
        cout << "--Virt From Square--" << endl;
        return lenght * lenght;
    }
    const void printInfo(){
        cout << "This is. Its params:" << endl 
        << "Side lenght:" << lenght << endl 
        << "Its area:" << areaVirt() << endl;
    }   
};

class Cube: public Square{
public:
    Cube(){
        lenght = 0;
        cout << "ctor Cube(empty), lenght = " << lenght << endl;
    }
    Cube(float _lenght): Square(_lenght){
        //Square::lenght = _lenght;
        cout << "ctor Cube, lenght = " << lenght << endl;
    }
    ~Cube(){
        cout << "dtor Cube" << lenght << endl;
    }
    //Считаю, что лучше переопределить не для вычисления объёма, а для вычисления площади поверхности
    float area const(){
        cout << "--From Cube--" << endl;
        return 6 * Square::area();
    }
    float areaVirt const(){
        cout << "--Virt From Cube--" << endl;
        return 6 * Square::areaVirt();
    }
    const bool operator == (Cube& cube){
        return (Square::lenght == cube.Square::lenght);
    }
};

/*
Type <class Type>;
class Vector{
private:
    Type* arr;
    int size;
public:
    Vector (){
        arr = nullptr;
        size = 0;
    }
    Vector (Type* _arr, int _size){
        arr = new Type [_size];
        for (int index = 0; index < _size; index++){
            arr[index] = _arr[index];    
        }
        size = _size;
     }
     ~Vector(){
        delete[] arr;
    }
    Type operator = (int index){
        return arr[index];
    }
    Vector& operator + (Type obj){ //Add object to end of Vector
        size ++;
        arrNew = new Type[size];
        for (int i = 0; i < size - 1; i++){
            arrNew[i] = arr[i];
        }
        arrNew[size - 1] = obj;
        delete[] arr;

        arr = new Type[this -> size];
        for (int i = 0; i < size; i++){
            arr[i] = arrNew[i];
        }
        delete[] arrNew;
        return *this;
    }
    Vector& operator - (Type obj){ //remove last in obj from Vector
        bool isIn = false;
        int indexDel;
        for (int i = this -> size - 1; i >= 0; i--){
            isIn = (obj == arr[i]);
            if (isIn){
                indexDel = i;
                size --;
                arrNew = new Type[size];
                int j = 0;
                while (j < size){
                    bool isFind = False;
                    if (j != indexDel){
                        if (!isFind){
                            arrNew[j] = arr[j];
                            j++;
                        }
                        else{
                           arrNew[j] = arr[j + 1];
                           j++;
                        } 
                    }
                }//new array
                delete[] this -> arr;
                arr = new Type[this -> size];
                for (int j = 0; j < size; j++){
                    arr[j] = arrNew[j];
                }
                delete[] arrNew;
                return *this;
            }
        }
        return *this;
    }
    
    //Vector& operator & (Vector& vect){ }//intersection
    //Vector&  operator | (Vector& vect){ }//union
      
};
*/

int main(){
    Square* ptrSquare;
    Cube cubeS(10);
    ptrSquare = &cubeS;
    
    cout << "SQR" << endl;
    
    Cube* ptrCube;
    Cube cubeC(3);
    ptrCube = &cubeC;

    
    cout << "CB" << endl;
    
    cout << "Area (ptrSquare) is "<< ptrSquare -> area() << endl << endl;
    cout << "AreaVirt (ptrSquare) is "<< ptrSquare -> areaVirt() << endl << endl;
    
    cout << "Area (ptrCube) is "<< ptrCube -> area() << endl << endl;
    cout << "AreaVirt (ptrCube) is "<< ptrCube -> areaVirt() << endl << endl;

    return 0;   
};